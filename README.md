This is intended to be a classifier for the georgian alphabet as it appears in modern usage.
A data-set consisiting of fonts harvested from https://fonts.ge/ has been assembled.
Following a discussion at the Brum-AI meetup on 13/12/2018 (forgot his name) has suggested that handwritten characters can be reliably classified with classifiers constructed from various 
different electronic fonts, allowing a much cheaper, quicker and easier dataset than would otherwise be possible.